#!/bin/bash

ls $(pwd)

echo "Copying files to S3..."
aws --profile parris-home s3 cp ./index.html s3://www.twoandahalfamigos.com/ --acl public-read
aws --profile parris-home s3 cp ./logo.jpg s3://www.twoandahalfamigos.com/ --acl public-read
echo "Done copying to S3."